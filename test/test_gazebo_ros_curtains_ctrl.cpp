// Copyright 2020 Institute for Infocomm Research
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gazebo/test/ServerFixture.hh>
#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/string.hpp>
#include <geometry_msgs/msg/vector3.hpp>

#include <memory>
#include <string>

#define tol 10e-2

using namespace std::literals::chrono_literals; // NOLINT

class GazeboRemoveInsertModelTest : public gazebo::ServerFixture
{
};

TEST_F(GazeboRemoveInsertModelTest, Test_World_Loading)
{
  // Load test world and start paused
  this->Load("worlds/test_double_story.world", true);

  // Check the world
  auto world = gazebo::physics::get_world();
  ASSERT_NE(nullptr, world);

  // Check models
  auto ground_plane = world->ModelByName("ground_plane");
  ASSERT_NE(nullptr, ground_plane);

  auto cafe_level_1 = world->ModelByName("cafe_level_1");
  ASSERT_NE(nullptr, cafe_level_1);

  auto cafe_level_2 = world->ModelByName("cafe_level_2");
  ASSERT_NE(nullptr, cafe_level_2);

  auto cafe_table = world->ModelByName("cafe_table");
  ASSERT_NE(nullptr, cafe_table);

  auto cafe_table_2 = world->ModelByName("cafe_table_2");
  ASSERT_NE(nullptr, cafe_table_2);

  auto cafe_table_3 = world->ModelByName("cafe_table_3");
  ASSERT_NE(nullptr, cafe_table_3);

  auto cabinet = world->ModelByName("cabinet");
  ASSERT_NE(nullptr, cabinet);

  auto cabinet_2 = world->ModelByName("cabinet_2");
  ASSERT_NE(nullptr, cabinet_2);

  auto cabinet_3 = world->ModelByName("cabinet_3");
  ASSERT_NE(nullptr, cabinet_3);

  auto cabinet_4 = world->ModelByName("cabinet_4");
  ASSERT_NE(nullptr, cabinet_4);

  auto cabinet_5 = world->ModelByName("cabinet_5");
  ASSERT_NE(nullptr, cabinet_5);

  auto cabinet_6 = world->ModelByName("cabinet_6");
  ASSERT_NE(nullptr, cabinet_6);

  auto cabinet_7 = world->ModelByName("cabinet_7");
  ASSERT_NE(nullptr, cabinet_7);

  auto hospital_bed_1 = world->ModelByName("hospital_bed_1");
  ASSERT_NE(nullptr, hospital_bed_1);

  auto cabinet_near_bed_1 = world->ModelByName("cabinet_near_bed_1");
  ASSERT_NE(nullptr, cabinet_near_bed_1);

  auto hospital_curtain_half_opened_1 = world->ModelByName("hospital_curtain_half_opened_1");
  ASSERT_NE(nullptr, hospital_curtain_half_opened_1);

  auto hospital_bed_2 = world->ModelByName("hospital_bed_2");
  ASSERT_NE(nullptr, hospital_bed_2);

  auto cabinet_near_bed_2 = world->ModelByName("cabinet_near_bed_2");
  ASSERT_NE(nullptr, cabinet_near_bed_2);

  auto hospital_curtain_closed_2 = world->ModelByName("hospital_curtain_closed_2");
  ASSERT_NE(nullptr, hospital_curtain_closed_2);

  auto hospital_bed_3 = world->ModelByName("hospital_bed_3");
  ASSERT_NE(nullptr, hospital_bed_3);

  auto cabinet_near_bed_3 = world->ModelByName("cabinet_near_bed_3");
  ASSERT_NE(nullptr, cabinet_near_bed_3);

  auto hospital_bed_4 = world->ModelByName("hospital_bed_4");
  ASSERT_NE(nullptr, hospital_bed_4);

  auto cabinet_near_bed_4 = world->ModelByName("cabinet_near_bed_4");
  ASSERT_NE(nullptr, cabinet_near_bed_4);
}

TEST_F(GazeboRemoveInsertModelTest, Test_Elevator)
{
  // Load test world and start paused
  this->Load("worlds/test_double_story.world", true);

  // Check the world
  auto world = gazebo::physics::get_world();
  ASSERT_NE(nullptr, world);

  auto elevator = world->ModelByName("elevator");
  ASSERT_NE(nullptr, elevator);

  auto elevator_shaft = world->ModelByName("elevator_shaft");
  ASSERT_NE(nullptr, elevator_shaft);

  // Link
  auto elevator_link = elevator->GetLink("elevator_link");
  ASSERT_NE(nullptr, elevator_link);

  // Create node and executor
  auto node = std::make_shared<rclcpp::Node>("gazebo_ros_curtains_ctrl_test");
  ASSERT_NE(nullptr, node);

  rclcpp::executors::SingleThreadedExecutor executor;
  executor.add_node(node);

  // Publisher for elevator command
  auto pub = node->create_publisher<std_msgs::msg::String>(
    "test/elevator_demo", rclcpp::QoS(rclcpp::KeepLast(1)));

  auto msg = std_msgs::msg::String();
  msg.data = "1";

  double sleep = 0;
  double max_sleep = 100;
  for (; sleep < max_sleep; ++sleep) {
    pub->publish(msg);
    executor.spin_once(100ms);
    world->Step(100);
  }

  // Check model state
  EXPECT_NEAR(0.0, elevator_link->WorldPose().Pos().X(), tol);
  EXPECT_NEAR(0.0, elevator_link->WorldPose().Pos().Y(), tol);
  EXPECT_NEAR(3.075, elevator_link->WorldPose().Pos().Z(), tol);
}

TEST_F(GazeboRemoveInsertModelTest, Test_Curtain_1)
{
  // Load test world and start paused
  this->Load("worlds/test_double_story.world", true);

  // Check the world
  auto world = gazebo::physics::get_world();
  ASSERT_NE(nullptr, world);

  // Create node and executor
  auto node = std::make_shared<rclcpp::Node>("gazebo_ros_curtains_ctrl_test");
  ASSERT_NE(nullptr, node);

  rclcpp::executors::SingleThreadedExecutor executor;
  executor.add_node(node);

  // Publisher for curtain 1 command
  auto pub_1 = node->create_publisher<geometry_msgs::msg::Vector3>(
    "curtain_control_1", rclcpp::QoS(rclcpp::KeepLast(1)));

  auto msg_1 = geometry_msgs::msg::Vector3();

  // Curtain_1 in open state
  msg_1.x = 1;
  msg_1.y = 0;
  msg_1.z = 0;

  double sleep = 0;
  double max_sleep = 10;
  for (; sleep < max_sleep; ++sleep) {
    pub_1->publish(msg_1);
    executor.spin_once(100ms);
    world->Step(100);
  }

  auto hospital_curtain_opened_1 = world->ModelByName("hospital_curtain_opened_1");
  ASSERT_NE(nullptr, hospital_curtain_opened_1);

  // Curtain_1 in half open state
  msg_1.x = 0;
  msg_1.y = 1;
  msg_1.z = 0;

  sleep = 0;
  for (; sleep < max_sleep; ++sleep) {
    pub_1->publish(msg_1);
    executor.spin_once(100ms);
    world->Step(100);
  }

  auto hospital_curtain_half_opened_1_n = world->ModelByName("hospital_curtain_half_opened_1");
  ASSERT_NE(nullptr, hospital_curtain_half_opened_1_n);

  // Curtain_1 in closed state
  msg_1.x = 0;
  msg_1.y = 0;
  msg_1.z = 1;

  sleep = 0;
  for (; sleep < max_sleep; ++sleep) {
    pub_1->publish(msg_1);
    executor.spin_once(100ms);
    world->Step(100);
  }

  auto hospital_curtain_closed_1 = world->ModelByName("hospital_curtain_closed_1");
  ASSERT_NE(nullptr, hospital_curtain_closed_1);
}

TEST_F(GazeboRemoveInsertModelTest, Test_Curtain_2)
{
  // Load test world and start paused
  this->Load("worlds/test_double_story.world", true);

  // Check the world
  auto world = gazebo::physics::get_world();
  ASSERT_NE(nullptr, world);

  // Create node and executor
  auto node = std::make_shared<rclcpp::Node>("gazebo_ros_curtains_ctrl_test");
  ASSERT_NE(nullptr, node);

  rclcpp::executors::SingleThreadedExecutor executor;
  executor.add_node(node);

  // Publisher for curtain 2 command
  auto pub_2 = node->create_publisher<geometry_msgs::msg::Vector3>(
    "curtain_control_2", rclcpp::QoS(rclcpp::KeepLast(1)));

  auto msg_2 = geometry_msgs::msg::Vector3();

  // Curtain_2 in open state
  msg_2.x = 1;
  msg_2.y = 0;
  msg_2.z = 0;

  double sleep = 0;
  double max_sleep = 10;
  for (; sleep < max_sleep; ++sleep) {
    pub_2->publish(msg_2);
    executor.spin_once(100ms);
    world->Step(100);
  }

  auto hospital_curtain_opened_2 = world->ModelByName("hospital_curtain_opened_2");
  ASSERT_NE(nullptr, hospital_curtain_opened_2);

  // Curtain_2 in half open state
  msg_2.x = 0;
  msg_2.y = 1;
  msg_2.z = 0;

  sleep = 0;
  for (; sleep < max_sleep; ++sleep) {
    pub_2->publish(msg_2);
    executor.spin_once(100ms);
    world->Step(100);
  }

  auto hospital_curtain_half_opened_2 = world->ModelByName("hospital_curtain_half_opened_2");
  ASSERT_NE(nullptr, hospital_curtain_half_opened_2);

  // Curtain_1 in closed state
  msg_2.x = 0;
  msg_2.y = 0;
  msg_2.z = 1;

  sleep = 0;
  for (; sleep < max_sleep; ++sleep) {
    pub_2->publish(msg_2);
    executor.spin_once(100ms);
    world->Step(100);
  }

  auto hospital_curtain_closed_2_n = world->ModelByName("hospital_curtain_closed_2");
  ASSERT_NE(nullptr, hospital_curtain_closed_2_n);
}

int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
