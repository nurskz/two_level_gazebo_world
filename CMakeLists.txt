cmake_minimum_required(VERSION 3.5)
project(two_level_gazebo_world)

# Default to C99
if(NOT CMAKE_C_STANDARD)
  set(CMAKE_C_STANDARD 99)
endif()

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# find dependencies
find_package(ament_cmake REQUIRED)
# uncomment the following section in order to fill in further dependencies manually.
# find_package(<dependency> REQUIRED)
# find_package(gazebo_dev REQUIRED)
# find_package(gazebo_msgs REQUIRED)
find_package(rclcpp REQUIRED)
find_package(gazebo_ros REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(std_msgs REQUIRED)

# link_directories(${gazebo_dev_LIBRARY_DIRS})

include_directories(
  include
  ${rclcpp_INCLUDE_DIRS}
  ${geometry_msgs_INCLUDE_DIRS}
  ${std_msgs_INCLUDE_DIRS}
)

# include(CheckSymbolExists)
# check_symbol_exists(drand48 stdlib.h HAVE_DRAND48)
# link_directories(${gazebo_dev_LIBRARY_DIRS})


#add libraries
add_library(gazebo_ros_curtains_ctrl SHARED
  src/gazebo_ros_curtains_ctrl.cpp
)
target_include_directories(gazebo_ros_curtains_ctrl PUBLIC include)
ament_target_dependencies(gazebo_ros_curtains_ctrl
  "rclcpp"
  "gazebo_ros"
  "geometry_msgs"
)

# install
# install(TARGETS gazebo_ros_curtains_ctrl
#   ARCHIVE DESTINATION lib
#   LIBRARY DESTINATION lib
#   RUNTIME DESTINATION bin
# )

ament_export_libraries(gazebo_ros_curtains_ctrl)
ament_export_dependencies(gazebo_ros_curtains_ctrl)

install(DIRECTORY launch DESTINATION share/${PROJECT_NAME})
install(DIRECTORY worlds DESTINATION share/${PROJECT_NAME})
install(DIRECTORY models DESTINATION share/${PROJECT_NAME})


if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  # the following line skips the linter which checks for copyrights
  # uncomment the line when a copyright and license is not present in all source files
  #set(ament_cmake_copyright_FOUND TRUE)
  # the following line skips cpplint (only works in a git repo)
  # uncomment the line when this package is not in a git repo
  #set(ament_cmake_cpplint_FOUND TRUE)
  ament_lint_auto_find_test_dependencies()

  # find_package(ament_cmake_gtest REQUIRED)
  # add_subdirectory(test)
endif()


install(TARGETS gazebo_ros_curtains_ctrl
  DESTINATION share/${PROJECT_NAME})

install(DIRECTORY include/
  DESTINATION include/
)

ament_export_include_directories(include)
ament_export_dependencies(rclcpp)
# ament_export_dependencies(gazebo_dev)
# ament_export_dependencies(gazebo_msgs)
#CATKIN_DEPENDS
ament_export_dependencies(gazebo_ros)

ament_package()
