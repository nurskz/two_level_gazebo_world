// Copyright 2020 Institute for Infocomm Research
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "two_level_gazebo_world/gazebo_ros_curtains_ctrl.hpp"

#include <memory>

namespace gazebo
{

class RemoveInsertModelPrivate
{
public:
  /// A pointer to the gazebo-ROS node
  gazebo_ros::Node::SharedPtr ros_node_;

  /// ROS subscribers
  rclcpp::Subscription<geometry_msgs::msg::Vector3>::SharedPtr rosSub_1;
  rclcpp::Subscription<geometry_msgs::msg::Vector3>::SharedPtr rosSub_2;
};

RemoveInsertModel::RemoveInsertModel()
: impl_(std::make_unique<RemoveInsertModelPrivate>())
{
}

RemoveInsertModel::~RemoveInsertModel()
{
}


/// \brief The load function is called by Gazebo when the plugin is
/// inserted into simulation
/// \param[in] _parent A pointer to the model/world that this plugin is
/// attached to.
/// \param[in] _sdf A pointer to the plugin's SDF element.
void RemoveInsertModel::Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
{
  // Store the world pointer for convenience.
  this->my_world = _parent;

  // Initialize ROS node
  impl_->ros_node_ = gazebo_ros::Node::Get(_sdf);

  // Get QoS profiles
  const gazebo_ros::QoS & qos = impl_->ros_node_->get_qos();

  impl_->rosSub_1 = impl_->ros_node_->create_subscription<geometry_msgs::msg::Vector3>(
    "curtain_control_1",
    qos.get_subscription_qos("curtain_control_1", rclcpp::QoS(1)),
    std::bind(&RemoveInsertModel::OnRosMsg_1, this, std::placeholders::_1));

  impl_->rosSub_2 = impl_->ros_node_->create_subscription<geometry_msgs::msg::Vector3>(
    "curtain_control_2",
    qos.get_subscription_qos("curtain_control_2", rclcpp::QoS(1)),
    std::bind(&RemoveInsertModel::OnRosMsg_2, this, std::placeholders::_1));
}

/// \brief Handle an incoming message from ROS
/// \param[in] _msg_1 A float value that is used to set the state of curtains
void RemoveInsertModel::OnRosMsg_1(const geometry_msgs::msg::Vector3::ConstSharedPtr _msg_1)
{
  sdf::SDF modelSDF;  // For inserting model from the string

  curtain_opened_1 = this->my_world->ModelByName("hospital_curtain_opened_1");
  curtain_half_opened_1 = this->my_world->ModelByName("hospital_curtain_half_opened_1");
  curtain_closed_1 = this->my_world->ModelByName("hospital_curtain_closed_1");

  if (_msg_1->x == 1 && _msg_1->y == 0 && _msg_1->z == 0) {  // Opening curtains
    if (curtain_closed_1 != NULL) {
      this->my_world->RemoveModel(curtain_closed_1);
    }
    if (curtain_half_opened_1 != NULL) {
      this->my_world->RemoveModel(curtain_half_opened_1);
    }
    if (curtain_opened_1 == NULL) {
      // Insert a model from string
      modelSDF.SetFromString(
        R"(<sdf version ='1.4'>\
        <model name='hospital_curtain_opened_1'>\
          <pose frame=''>10.45 -6.58 0.0 0 0 3.14159</pose>\
          <include>\
            <uri>model://aws_robomaker_hospital_curtain_open_01</uri>\
          </include>\
        </model>\
        </sdf>)");
      // Demonstrate using a custom model name.
      this->my_world->InsertModelSDF(modelSDF);
    }
  } else if (_msg_1->x == 0 && _msg_1->y == 1 && _msg_1->z == 0) {  // Half opening curtains
    if (curtain_closed_1 != NULL) {
      this->my_world->RemoveModel(curtain_closed_1);
    }
    if (curtain_opened_1 != NULL) {
      this->my_world->RemoveModel(curtain_opened_1);
    }
    if (curtain_half_opened_1 == NULL) {
      // Insert a model from string
      modelSDF.SetFromString(
        R"(<sdf version ='1.4'>\
        <model name='hospital_curtain_half_opened_1'>\
          <pose frame=''>10.45 -6.58 0.0 0 0 3.14159</pose>\
          <include>\
            <uri>model://aws_robomaker_hospital_curtain_half_open_01</uri>\
          </include>\
        </model> \
        </sdf>)");
      // Demonstrate using a custom model name.
      this->my_world->InsertModelSDF(modelSDF);
    }
  } else if (_msg_1->x == 0 && _msg_1->y == 0 && _msg_1->z == 1) {   // Closing curtains
    if (curtain_half_opened_1 != NULL) {
      this->my_world->RemoveModel(curtain_half_opened_1);
    }
    if (curtain_opened_1 != NULL) {
      this->my_world->RemoveModel(curtain_opened_1);
    }
    if (curtain_closed_1 == NULL) {
      // Insert a model from string
      modelSDF.SetFromString(
        R"(<sdf version ='1.4'>\
        <model name='hospital_curtain_closed_1'>\
          <pose frame=''>10.45 -6.58 0.0 0 0 3.14159</pose>\
          <include>\
            <uri>model://aws_robomaker_hospital_curtain_half_open_01</uri>\
          </include>\
        </model>\
        </sdf>)");
      // Demonstrate using a custom model name.
      this->my_world->InsertModelSDF(modelSDF);
    }
  }
}

/// \brief Handle an incoming message from ROS
/// \param[in] _msg_2 A float value that is used to set the state of curtains
void RemoveInsertModel::OnRosMsg_2(const geometry_msgs::msg::Vector3::ConstSharedPtr _msg_2)
{
  sdf::SDF modelSDF;   // For inserting model from the string
  curtain_opened_2 = this->my_world->ModelByName("hospital_curtain_opened_2");
  curtain_half_opened_2 = this->my_world->ModelByName("hospital_curtain_half_opened_2");
  curtain_closed_2 = this->my_world->ModelByName("hospital_curtain_closed_2");

  if (_msg_2->x == 1 && _msg_2->y == 0 && _msg_2->z == 0) {        // Opening curtains
    if (curtain_closed_2 != NULL) {
      this->my_world->RemoveModel(curtain_closed_2);
    }
    if (curtain_half_opened_2 != NULL) {
      this->my_world->RemoveModel(curtain_half_opened_2);
    }
    if (curtain_opened_2 == NULL) {
      // Insert a model from string
      modelSDF.SetFromString(
        R"(<sdf version ='1.4'>\
        <model name='hospital_curtain_opened_2'>\
          <pose frame=''>6.45 -6.58 0.0 0 0 3.14159</pose>\
          <include>\
            <uri>model://aws_robomaker_hospital_curtain_open_01</uri>\
          </include>\
        </model>\
        </sdf>)");
      // Demonstrate using a custom model name.
      this->my_world->InsertModelSDF(modelSDF);
    }
  } else if (_msg_2->x == 0 && _msg_2->y == 1 && _msg_2->z == 0) {  // Half opening curtains
    if (curtain_closed_2 != NULL) {
      this->my_world->RemoveModel(curtain_closed_2);
    }
    if (curtain_opened_2 != NULL) {
      this->my_world->RemoveModel(curtain_opened_2);
    }
    if (curtain_half_opened_2 == NULL) {
      // Insert a model from string
      modelSDF.SetFromString(
        R"(<sdf version ='1.4'>\
        <model name='hospital_curtain_half_opened_2'>\
          <pose frame=''>6.45 -6.58 0.0 0 0 3.14159</pose>\
          <include>\
            <uri>model://aws_robomaker_hospital_curtain_half_open_01</uri>\
          </include>\
        </model> \
        </sdf>)");
      // Demonstrate using a custom model name.
      this->my_world->InsertModelSDF(modelSDF);
    }
  } else if (_msg_2->x == 0 && _msg_2->y == 0 && _msg_2->z == 1) {   // Closing curtains
    if (curtain_half_opened_2 != NULL) {
      this->my_world->RemoveModel(curtain_half_opened_2);
    }
    if (curtain_opened_2 != NULL) {
      this->my_world->RemoveModel(curtain_opened_2);
    }
    if (curtain_closed_2 == NULL) {
      // Insert a model from string
      modelSDF.SetFromString(
        R"(<sdf version ='1.4'>\
        <model name='hospital_curtain_closed_2'>\
          <pose frame=''>6.45 -6.58 0.0 0 0 3.14159</pose>\
          <include>\
            <uri>model://aws_robomaker_hospital_curtain_closed_01</uri>\
          </include>\
        </model>\
        </sdf>)");
      // Demonstrate using a custom model name.
      this->my_world->InsertModelSDF(modelSDF);
    }
  }
}

// Tell Gazebo about this plugin, so that Gazebo can call Load on this plugin.
GZ_REGISTER_WORLD_PLUGIN(RemoveInsertModel)
}  // namespace gazebo
