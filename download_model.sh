#!/bin/bash
mkdir models
cd models

# sudo apt install subversion

printf "Downloading gazebo models:" #  from "https://github.com/osrf/gazebo_models"
svn checkout https://github.com/osrf/gazebo_models/trunk/cafe
svn checkout https://github.com/osrf/gazebo_models/trunk/cabinet
svn checkout https://github.com/osrf/gazebo_models/trunk/cafe_table
svn checkout https://github.com/osrf/gazebo_models/trunk/ground_plane
svn checkout https://github.com/osrf/gazebo_models/trunk/sun
# svn checkout https://github.com/osrf/gazebo_models/trunk/cafe_counter

printf "Downloading curtains from aws:" # from "https://github.com/aws-robotics/aws-robomaker-hospital-world/tree/master/models"
svn checkout https://github.com/aws-robotics/aws-robomaker-hospital-world/trunk/models/aws_robomaker_hospital_curtain_closed_01
svn checkout https://github.com/aws-robotics/aws-robomaker-hospital-world/trunk/models/aws_robomaker_hospital_curtain_half_open_01
svn checkout https://github.com/aws-robotics/aws-robomaker-hospital-world/trunk/models/aws_robomaker_hospital_curtain_open_01

cd ..
printf "Downloading hospital bed from ignitionrobotics..." # "https://app.ignitionrobotics.org/OpenRobotics/fuel/models/CGMClassic"
python3 fuel_model_download.py

printf "Updating GAZEBO_MODEL_PATH with downloaded models\n"
echo "export GAZEBO_MODEL_PATH=`pwd`/models:" >> ~/.bashrc
printf "All done!\n"