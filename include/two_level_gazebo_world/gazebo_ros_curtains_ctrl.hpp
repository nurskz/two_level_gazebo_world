// Copyright 2020 Institute for Infocomm Research
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TWO_LEVEL_GAZEBO_WORLD__GAZEBO_ROS_CURTAINS_CTRL_HPP_
#define TWO_LEVEL_GAZEBO_WORLD__GAZEBO_ROS_CURTAINS_CTRL_HPP_

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo_ros/node.hpp>

#include <memory>

#include "geometry_msgs/msg/vector3.hpp"

namespace gazebo
{

class RemoveInsertModelPrivate;

/// \brief A plugin to control curtains state
class RemoveInsertModel : public WorldPlugin
{
public:
  /// \brief Constructor
  RemoveInsertModel();

  /// \brief Destructor
  ~RemoveInsertModel();

public:
  /// \brief The load function is called by Gazebo when the plugin is
  /// inserted into simulation
  /// \param[in] _parent A pointer to the model/world that this plugin is
  /// attached to.
  /// \param[in] _sdf A pointer to the plugin's SDF element.
  void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf);

  /// \brief Handle an incoming message from ROS
  /// \param[in] _msg_1 A float value that is used to set the state of curtains
  void OnRosMsg_1(const geometry_msgs::msg::Vector3::ConstSharedPtr _msg_1);

  /// \brief Handle an incoming message from ROS
  /// \param[in] _msg_2 A float value that is used to set the state of curtains
  void OnRosMsg_2(const geometry_msgs::msg::Vector3::ConstSharedPtr _msg_2);

private:
  physics::WorldPtr my_world;    /// \brief Pointer to the world.

/// \brief Pointer to the models

private:
  physics::ModelPtr curtain_opened_1;

private:
  physics::ModelPtr curtain_half_opened_1;

private:
  physics::ModelPtr curtain_closed_1;

private:
  physics::ModelPtr curtain_opened_2;

private:
  physics::ModelPtr curtain_half_opened_2;

private:
  physics::ModelPtr curtain_closed_2;

private:
  std::unique_ptr<RemoveInsertModelPrivate> impl_;  /// Private data pointer
};
}  // namespace gazebo
#endif  // TWO_LEVEL_GAZEBO_WORLD__GAZEBO_ROS_CURTAINS_CTRL_HPP_
