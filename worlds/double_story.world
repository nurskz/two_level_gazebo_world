<?xml version="1.0" ?>
<!-- Prepared by chanjianle and nursultan -->
<sdf version='1.6'>
  <world name='double_story_world'>

    <include>
      <uri>model://sun</uri>
    </include>

    <include>
      <uri>model://ground_plane</uri>
    </include>

    <scene>
      <ambient>0.4 0.4 0.4 1</ambient>
      <background>0.7 0.7 0.7 1</background>
      <shadows>true</shadows>
    </scene>

    <physics name='default_physics' type="ode">
      <real_time_update_rate>1000.0</real_time_update_rate>
      <max_step_size>0.001</max_step_size>
      <real_time_factor>1</real_time_factor>
      <ode>
        <solver>
          <type>quick</type>
          <iters>150</iters>
          <precon_iters>0</precon_iters>
          <sor>1.40</sor>
          <use_dynamic_moi_rescaling>1</use_dynamic_moi_rescaling>
        </solver>
        <constraints>
          <cfm>0.00001</cfm>
          <erp>0.2</erp>
          <contact_max_correcting_vel>2000.00</contact_max_correcting_vel>
          <contact_surface_layer>0.010</contact_surface_layer>
        </constraints>
      </ode>
    </physics>
    
    <gui fullscreen='0'>
      <camera name='user_camera'>
        <pose>12.3704 -5.78376 31.8232 2.7e-05 1.5698 1.56803</pose>
        <view_controller>orbit</view_controller>
        <projection_type>perspective</projection_type>
      </camera>
    </gui>    

    <model name="cafe_level_1">
      <pose>13.3372 -3.81199 -0.039929 0 0 -1.5708</pose>
      <static>1</static>
      <include>
        <uri>model://cafe</uri>
      </include>
    </model>
    
    <model name='elevator'>
      <pose>0 0 0.075 0 0 0</pose>
      <link name='elevator_link'>
        <inertial>
          <mass>800</mass>
          <pose>0 0 0 0 0 0</pose>
          <inertia>
            <ixx>1</ixx>
            <ixy>0</ixy>
            <ixz>0</ixz>
            <iyy>1</iyy>
            <iyz>0</iyz>
            <izz>1</izz>
          </inertia>
        </inertial>
        <collision name='floor_collision'>
          <geometry>
            <box>
              <size>2.25 2.25 0.15</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='floor_visual'>
          <geometry>
            <box>
              <size>2.25 2.25 0.15</size>
            </box>
          </geometry>
        </visual>
        <collision name='wall_1_collision'>
          <pose>1.0745 0.5725 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.1 1.15 2.25</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='wall_1_visual'>
          <pose>1.0745 0.5725 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.1 1.15 2.25</size>
            </box>
          </geometry>
          <material>
            <script>
              <name>Gazebo/Blue</name>
              <uri>__default__</uri>
            </script>
          </material>
        </visual>
        <collision name='wall_2_collision'>
          <pose>1.0745 -1.0625 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.1 0.125 2.25</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='wall_2_visual'>
          <pose>1.0745 -1.0625 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.1 0.125 2.25</size>
            </box>
          </geometry>
          <material>
            <script>
              <name>Gazebo/Blue</name>
              <uri>__default__</uri>
            </script>
          </material>
        </visual>
        <collision name='wall_1_collision_clone'>
          <pose>-1.0745 0.5725 1.125 0 0 0</pose>
          <geometry>
            <box>
              <size>0.1 1.15 2.25</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='wall_1_visual_clone'>
          <pose>-1.0745 0.5725 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.1 1.15 2.25</size>
            </box>
          </geometry>
          <material>
            <script>
              <name>Gazebo/Blue</name>
              <uri>__default__</uri>
            </script>
          </material>
        </visual>
        <collision name='wall_2_collision_clone'>
          <pose>-1.0745 -1.0625 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.1 0.125 2.25</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='wall_2_visual_clone'>
          <pose>-1.0745 -1.0625 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.1 0.125 2.25</size>
            </box>
          </geometry>
          <material>
            <script>
              <name>Gazebo/Blue</name>
              <uri>__default__</uri>
            </script>
          </material>
        </visual>
      </link>

      <link name='door_link'>
        <pose>0 0 0 0 0 0</pose>
        <inertial>
          <pose>0 0 0 0 -0 0</pose>
          <inertia>
            <ixx>1</ixx>
            <ixy>0</ixy>
            <ixz>0</ixz>
            <iyy>1</iyy>
            <iyz>0</iyz>
            <izz>1</izz>
          </inertia>
          <mass>1</mass>
        </inertial>
        <collision name='collision_door'>
          <pose>1.0745 -0.5 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.08 1 2.25</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='visual_door'>
          <pose>1.0745 -0.5 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.08 1 2.25</size>
            </box>
          </geometry>
        </visual>
        <collision name='collision_door_clone'>
          <pose>-1.0745 -0.5 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.08 1 2.25</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='visual_door_clone'>
          <pose>-1.0745 -0.5 1.125 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.08 1 2.25</size>
            </box>
          </geometry>
        </visual>
      </link>

      <joint name='door' type='prismatic'>
        <parent>elevator_link</parent>
        <child>door_link</child>
        <axis>
          <xyz>0 1 0</xyz>
          <limit>
            <lower>0</lower>
            <upper>1</upper>
            <effort>10</effort>
          </limit>
          <dynamics>
            <damping>2</damping>
            <spring_reference>0</spring_reference>
            <spring_stiffness>0</spring_stiffness>
          </dynamics>
          <use_parent_model_frame>1</use_parent_model_frame>
        </axis>
      </joint>
      <joint name='lift' type='prismatic'>
        <parent>world</parent>
        <child>elevator_link</child>
        <axis>
          <xyz>0 0 1</xyz>
          <limit>
            <lower>0</lower>
            <upper>10</upper>
            <effort>100000</effort>
          </limit>
          <dynamics>
            <damping>50</damping>
            <spring_reference>0</spring_reference>
            <spring_stiffness>0</spring_stiffness>
          </dynamics>
          <use_parent_model_frame>1</use_parent_model_frame>
        </axis>
        <physics>
          <ode>
            <implicit_spring_damper>1</implicit_spring_damper>
            <limit>
              <cfm>0</cfm>
              <erp>0.2</erp>
            </limit>
          </ode>
        </physics>
      </joint>
      <plugin name='elevator_plugin' filename='libgazebo_ros_elevator.so'>
        <ros>
          <namespace>demo</namespace>
          <remapping>elevator:=elevator_demo</remapping>
        </ros>
        <lift_joint>elevator::lift</lift_joint>
        <door_joint>elevator::door</door_joint>
        <floor_height>3.075</floor_height>
        <bottom_floor>0</bottom_floor>
        <top_floor>1</top_floor>
        <door_wait_time>5</door_wait_time> <!-- Time the elevator door will stay open in seconds -->
      </plugin>
    </model>

    <model name='elevator_shaft'>
      <static>1</static>
      <link name='link'>
        <collision name='shaft_wall_1_collision'>
          <pose>0 1.25 3 0 -0 0</pose>
          <geometry>
            <box>
              <size>2.5 0.15 6</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='shaft_wall_1_visual'>
          <pose>0 1.25 3 0 -0 0</pose>
          <geometry>
            <box>
              <size>2.5 0.15 6</size>
            </box>
          </geometry>
        </visual>

        <collision name='shaft_wall_2_collision'>
          <pose>0 -1.25 3 0 -0 0</pose>
          <geometry>
            <box>
              <size>2.5 0.15 6</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='shaft_wall_2_visual'>
          <pose>0 -1.25 3 0 -0 0</pose>
          <geometry>
            <box>
              <size>2.5 0.15 6</size>
            </box>
          </geometry>
        </visual>

        <collision name='shaft_wall_lvl1_col_1'>
          <pose>1.75 1.25 1.5 0 -0 0</pose>
          <geometry>
            <box>
              <size>1.0 0.15 3.0</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='shaft_wall_lvl1_visual_1'>
          <pose>1.75 1.25 1.5 0 -0 0</pose>
          <geometry>
            <box>
              <size>1.0 0.15 3.0</size>
            </box>
          </geometry>
        </visual>

        <collision name='shaft_wall_lvl1_col_2'>
          <pose>1.75 -1.25 1.5 0 -0 0</pose>
          <geometry>
            <box>
              <size>1.0 0.15 3.0</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='shaft_wall_lvl1_visual_2'>
          <pose>1.75 -1.25 1.5 0 -0 0</pose>
          <geometry>
            <box>
              <size>1.0 0.15 3.0</size>
            </box>
          </geometry>
        </visual>

        <collision name='shaft_wall_lvl2_col_1'>
          <pose>-1.77 1.25 4.5 0 -0 0</pose>
          <geometry>
            <box>
              <size>1.02 0.15 3.0</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='shaft_wall_lvl2_visual_1'>
          <pose>-1.77 1.25 4.5 0 -0 0</pose>
          <geometry>
            <box>
              <size>1.02 0.15 3.0</size>
            </box>
          </geometry>
        </visual>

        <collision name='shaft_wall_lvl2_col_2'>
          <pose>-1.77 -1.25 4.5 0 -0 0</pose>
          <geometry>
            <box>
              <size>1.02 0.15 3.0</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='shaft_wall_lvl2_visual_2'>
          <pose>-1.77 -1.25 4.5 0 -0 0</pose>
          <geometry>
            <box>
              <size>1.02 0.15 3.0</size>
            </box>
          </geometry>
        </visual>

        <collision name='shaft_wall_lvl2_col_3'>
          <pose>1.23 0 4.5 0 0 1.570796</pose>
          <geometry>
            <box>
              <size>2.60 0.15 3.0</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='shaft_wall_lvl2_visual_3'>
          <pose>1.23 0 4.5 0 0 1.570796</pose>
          <transparency>0.7</transparency>>
          <geometry>
            <box>
              <size>2.60 0.15 3.0</size>
            </box>
          </geometry>
        </visual>

        <collision name='shaft_wall_lvl1_col_3'>
          <pose>-1.23 0 1.5 0 0 1.570796</pose>
          <geometry>
            <box>
              <size>2.60 0.15 3.0</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='shaft_wall_lvl1_visual_3'>
          <pose>-1.23 0 1.5 0 0 1.570796</pose>
          <transparency>0.7</transparency>>
          <geometry>
            <box>
              <size>2.60 0.15 3.0</size>
            </box>
          </geometry>
        </visual>

        <collision name='floor0_collision'>
          <pose>1.19 0 0.075 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.12 2.5 0.03</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='floor0_visual'>
          <pose>1.19 0 0.075 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.12 2.5 0.03</size>
            </box>
          </geometry>
        </visual>
        <collision name='floor1_collision'>
          <pose>-1.205 0 3.075 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.12 2.5 0.03</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='floor1_visual'>
          <pose>-1.205 0 3.075 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.12 2.5 0.03</size>
            </box>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <enable_wind>0</enable_wind>
        <kinematic>0</kinematic>
      </link>
    </model>

    <model name="cafe_level_2">
      <pose>-13.399 3.81145 2.95797 0 0 1.5708</pose>
      <static>1</static>
      <include>
        <uri>model://cafe</uri>
      </include>
    </model>

    <model name="cafe_table">
      <pose>-6.38195 4.795 3.20569 0 0 0</pose>
      <static>1</static>
      <include>
        <uri>model://cafe_table</uri>
      </include>
    </model>

    <model name="cafe_table_2">
      <pose>-6.55075 2.63355 3.20569 0 0 0</pose>
      <static>1</static>
      <include>
        <uri>model://cafe_table</uri>
      </include>
    </model>

    <model name="cafe_table_3">
      <pose>-10.4248 2.69395 3.20569 0 0 0</pose>
      <static>1</static>
      <include>
        <uri>model://cafe_table</uri>
      </include>
    </model>

    <model name="cabinet">
      <pose>-13.6156 5.67406 3.21021 0 0 0</pose>
      <static>1</static>
      <include>
        <uri>model://cabinet</uri>
      </include>
    </model>

    <model name="cabinet_2">
      <pose>-13.6123 5.18655 3.21021 0 0 0</pose>
      <static>1</static>
      <include>
        <uri>model://cabinet</uri>
      </include>
    </model>

    <model name="cabinet_3">
      <pose>-13.6128 4.7051 3.21021 0 0 0</pose>
      <static>1</static>
      <include>
        <uri>model://cabinet</uri>
      </include>
    </model>

    <model name="cabinet_4">
      <pose>-13.6035 4.2192 3.21021 0 0 0</pose>
      <static>1</static>
      <include>
        <uri>model://cabinet</uri>
      </include>
    </model>
    
    <model name="cabinet_5">
      <pose>-13.6028 3.73939 3.21021 0 0 0</pose>
      <static>1</static>
      <include>
        <uri>model://cabinet</uri>
      </include>
    </model>

    <model name="cabinet_6">
      <pose>-13.5911 3.26309 3.21021 0 0 0</pose>
      <static>1</static>
      <include>
        <uri>model://cabinet</uri>
      </include>
    </model>
    
    <model name="cabinet_7">
      <pose>-13.5964 2.80376 3.21021 0 -0 0</pose>
      <static>1</static>
      <include>
        <uri>model://cabinet</uri>
      </include>
    </model>
  

    <model name="hospital_bed_1">
      <pose>10.1 -6.8 0.46 0 0 1.570796</pose>
      <static>0</static>
      <include>
        <uri>model://CGMClassic</uri>
      </include>
    </model>
     <model name="cabinet_near_bed_1">
      <pose>9.2 -7.5 0.15 0 0 -1.570796</pose>
      <static>0</static>
      <include>
        <uri>model://cabinet</uri>
      </include>
    </model>
    <model name="hospital_curtain_half_opened_1">
      <pose>10.45 -6.58 0.0 0 0 3.14159</pose>
      <include>
        <uri>model://aws_robomaker_hospital_curtain_half_open_01</uri>
      </include>
    </model> 


    <model name="hospital_bed_2">
      <pose>6.1 -6.8 0.46 0 0 1.570796</pose>
      <static>0</static>
      <include>
        <uri>model://CGMClassic</uri>
      </include>
    </model>
    <model name="cabinet_near_bed_2">
      <pose>5.2 -7.5 0.15 0 0 -1.570796</pose>
      <static>0</static>
      <include>
        <uri>model://cabinet</uri>
      </include>
    </model>
    <model name="hospital_curtain_closed_2">
      <pose>6.45 -6.58 0.0 0 0 3.14159</pose>
      <include>
        <uri>model://aws_robomaker_hospital_curtain_closed_01</uri>
      </include>
    </model>  


    <model name="hospital_bed_3">
      <pose>10.1 -2.3 0.46 0 0 -1.570796</pose>
      <static>0</static>
      <include>
        <uri>model://CGMClassic</uri>
      </include>
    </model>
    <model name="cabinet_near_bed_3">
      <pose>9.2 -1.6 0.15 0 0 1.570796</pose>
      <static>0</static>
      <include>
        <uri>model://cabinet</uri>
      </include>    
    </model>

    <model name="hospital_bed_4">
      <pose>6.1 -2.3 0.46 0 0 -1.570796</pose>
      <static>0</static>
      <include>
        <uri>model://CGMClassic</uri>
      </include>
    </model>
    <model name="cabinet_near_bed_4">
      <pose>5.2 -1.6 0.15 0 0 1.570796</pose>
      <static>0</static>
      <include>
        <uri>model://cabinet</uri>
      </include>
    </model>

   <plugin name="curtain_control_plugin" filename="libgazebo_ros_curtains_ctrl.so"/>

  </world>
</sdf>