# Two Level Gazebo World

[![pipeline status](https://gitlab.com/nurskz/two_level_gazebo_world/badges/master/pipeline.svg)](https://gitlab.com/nurskz/two_level_gazebo_world/commits/master)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

## What is this?

This package is the two-level virtual world with the lift and hospital curtains developed for Gazebo simulator.

<img src="docs/img/two-level_world.png" alt="Two-level Gazebo world" align="left" style="zoom:60%;" />

The purpose of the virtual world with the lift and curtains in Gazebo simulator is to provide the user a virtual world environment that he/she can use for the training of industry staff and practicing various test scenarios with mobile robots. Inside the Gazebo world is a double story building equipped with a functional elevator that is capable of bringing the mobile robot to the next floor.  The following models are used to build this world:

- https://github.com/osrf/gazebo_models: *cafe, cabinet, cafe_table, ground_plane, sun*
- https://app.ignitionrobotics.org/fuel/models: *CGMClassic* (hospital bed)
- https://github.com/aws-robotics/aws-robomaker-hospital-world/tree/master/models:  *aws_robomaker_hospital_curtain_closed_01, aws_robomaker_hospital_curtain_half_open_01, aws_robomaker_hospital_curtain_open_01* (hospital curtains)

## Tested versions of Gazebo and ROS 

This gazebo world is tested with Gazebo 11 and ROS2 Foxy on Ubuntu 20.04. 



## **Setup**

This section lists steps on how to build **two_level_gazebo_world** package.

The built-in Gazebo plugin is used to control the elevator state. In order to interface with the aforementioned plugin, it is required to install `gazebo_ros_pkgs` as indicated in http://gazebosim.org/tutorials?tut=ros2_installing&cat=connect_ros. Hence, assuming that ROS2 Foxy debian packages are already installed, the `gazebo_ros_pkgs` packages are installed from debian packages (on Ubuntu) as follows:

```shell
sudo apt install ros-foxy-gazebo-ros-pkgs
```

Next, we can clone the package:

```shell
# Create ROS2 workspace
cd $HOME
mkdir -p gazebo_world_ros2_ws/src

# Download copy of two_level_gazebo_world
cd gazebo_world_ros2_ws/src
git clone https://gitlab.com/nurskz/two_level_gazebo_world.git
cd two_level_gazebo_world
```

After that we need to download the required model for gazebo world:

```shell
chmod +x download_model.sh
./download_model.sh
```

Once download is finished, the package is ready to build.



## Build & Launch

To build your package go to your workspace (`gazebo_world_ros2_ws`) :

```shell
cd $HOME/gazebo_world_ros2_ws
source /opt/ros/foxy/setup.bash
rosdep install --from-paths src --ignore-src -y --rosdistro $ROS_DISTRO
colcon build
source $HOME/gazebo_world_ros2_ws/install/local_setup.bash
```

To launch:

```shell
ros2 launch two_level_gazebo_world start_world.py 
```



## Run

### Control of Elevator state

To control the elevator state, we can simply publish floor number to the following topic: `/demo/elevator_demo `. For example, if we want to go to level 1: 

```shell
ros2 topic pub --once /demo/elevator_demo std_msgs/msg/String "{data: '1'}" 
```

After it reached to level 1, we can return to level 0 by publishing the following command: 

```shell
ros2 topic pub --once /demo/elevator_demo std_msgs/msg/String "{data: '0'}" 
```



### Control of Curtains state 

To control the curtain state, we need to publish `geometry_msgs::msg::Vector3` msg to the following topics:

- `/curtain_control_1 ` for the first curtain
- `/curtain_control_2 ` for the second curtain

The message format is the following:

- {1, 0, 0} - open curtains state
- {0, 1, 0} - half open curtains state
- {0, 0, 1} - closed curtains state

For example, the following command will change the first curtain state to open, if it is not already open:

```shell
ros2 topic pub --once /curtain_control_1 geometry_msgs/msg/Vector3 "{x: 1.0, y: 0.0, z: 0.0}"
```

 

