# Copyright 2020 Institute for Infocomm Research
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import requests
import logging
import zipfile
import io

if __name__ == '__main__':

    # Downloading hospital bed from ignitionrobotics
    fuel_model_name = "CGMClassic"

    url_fuel_model = '/'.join(["https://fuel.ignitionrobotics.org/1.0/OpenRobotics/models",
                              fuel_model_name])

    response = requests.get('%s.zip' % url_fuel_model, stream=True)
    if (response.status_code != 200):
        logging.error('Could not download the model. \
        Please visit to "https://app.ignitionrobotics.org/OpenRobotics/fuel/models/CGMClassic" \
        to manually download CGMClassic model')
    else:
        zp = zipfile.ZipFile(io.BytesIO(response.content))
        zp.extractall('/'.join(["./models", fuel_model_name]))
